package pk.labs.Lab9;

import pk.labs.Lab9.impl.ConsultationListFactoryImpl;
import pk.labs.Lab9.impl.TermImpl;
import pk.labs.Lab9.impl.ConsultationListImpl;
import pk.labs.Lab9.impl.ConsultationImpl;
import pk.labs.Lab9.beans.*;

public class LabDescriptor {
    
    public static Class<? extends Term> termBean = TermImpl.class;
    public static Class<? extends Consultation> consultationBean = ConsultationImpl.class;
    public static Class<? extends ConsultationList> consultationListBean = ConsultationListImpl.class;
    public static Class<? extends ConsultationListFactory> consultationListFactory = ConsultationListFactoryImpl.class;
    
}
